-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 24, 2015 at 09:22 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_tool`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_request`
--

CREATE TABLE IF NOT EXISTS `access_request` (
  `reqid` int(15) NOT NULL AUTO_INCREMENT,
  `reqdate` varchar(15) NOT NULL DEFAULT '',
  `username` varchar(32) NOT NULL DEFAULT '',
  `dbserver` varchar(3) NOT NULL DEFAULT '',
  `dbname` varchar(32) NOT NULL DEFAULT '',
  `tblname` varchar(32) NOT NULL DEFAULT '',
  `access` varchar(128) NOT NULL DEFAULT '',
  `duration` varchar(10) NOT NULL DEFAULT '',
  `remark` text,
  `grantedby` varchar(32) NOT NULL DEFAULT '',
  `grantdate` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`reqid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `access_request`
--

INSERT INTO `access_request` (`reqid`, `reqdate`, `username`, `dbserver`, `dbname`, `tblname`, `access`, `duration`, `remark`, `grantedby`, `grantdate`) VALUES
(5, '2015-11-21', 'fajar', 'loc', 'ci_ecommerce', 'module_aksi', 'select,insert,update,delete', '28-11-2015', 'test', '21-11-2015 04:18:11', '21-11-2015'),
(4, '2015-11-21', 'fajar', 'loc', 'db_litashop', 'tbl_menu', 'select,insert,update,delete', '28-11-2015', 'asa', '21-11-2015 02:44:14', '21-11-2015'),
(3, '2015-11-21', 'fajar', 'loc', 'db_litashop', 'tbl_kota', 'select,insert,update', '28-11-2015', '', '21-11-2015 02:43:27', '21-11-2015'),
(1, '2015-11-21', 'fajar', 'loc', 'ci_ecommerce', 'captcha', '', '28-11-2015', '', '21-11-2015 02:07:16', '21-11-2015'),
(2, '2015-11-21', 'fajar', 'loc', 'opencart', 'oc_category_filter', '', '28-11-2015', '', '21-11-2015 02:11:24', '21-11-2015');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `userpass` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `userpass`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `dbserver`
--

CREATE TABLE IF NOT EXISTS `dbserver` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dbname` varchar(20) DEFAULT NULL,
  `dbaddress` varchar(20) DEFAULT NULL,
  `dbuser` varchar(20) DEFAULT NULL,
  `dbpass` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `dbserver`
--

INSERT INTO `dbserver` (`id`, `dbname`, `dbaddress`, `dbuser`, `dbpass`) VALUES
(1, '250', '10.8.3.250', 'dev', 'dev'),
(2, '205', '10.8.3.205', 'dev', 'dev'),
(3, '88', '10.8.3.88', 'dev', 'dev'),
(4, 'local', 'localhost', 'root', '');

-- --------------------------------------------------------

--
-- Table structure for table `log_audit`
--

CREATE TABLE IF NOT EXISTS `log_audit` (
  `logid` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(15) NOT NULL DEFAULT '',
  `time` varchar(32) NOT NULL DEFAULT '',
  `action` varchar(50) NOT NULL DEFAULT '',
  `information` varchar(125) NOT NULL DEFAULT '',
  PRIMARY KEY (`logid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `log_audit`
--

INSERT INTO `log_audit` (`logid`, `user`, `time`, `action`, `information`) VALUES
(1, 'admin', '2015-11-23 11:05:55', 'logout', 'logout with IP address: ::1'),
(2, 'fajar', '2015-11-23 11:06:11', 'login', 'login with IP address: ::1'),
(3, 'admin', '2015-11-24 08:21:09', 'login', 'login with IP address: ::1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`) VALUES
(1, 'fajar', 'fajar.antono@mitracomm.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
