$(function(){   
  $(".price").on('keyup', function(){priceCurrency});priceCurrency();
  datepicker_format(); nav_url();
  
  //refresh inbox
  setTimeout("$('#inbox').load(site+'page/notifi/inbox')", 1200);
  $('#inbox').load(site+'page/notifi/inbox');

  //refresh persetujuan disposisi
  setTimeout("$('#p_disposisi').load(site+'page/notifi/p_disposisi')", 1200);
  $('#p_disposisi').load(site+'page/notifi/p_disposisi');

  //refresh persetujuan surat keluar
  setTimeout("$('#notif_surat_keluar').load(site+'page/notifi/p_surat_keluar')", 1200);
  $('#notif_surat_keluar').load(site+'page/notifi/p_surat_keluar');

  //refresh persetujuan surat keputusan
  setTimeout("$('#notif_surat_keputusan').load(site+'page/notifi/p_surat_keputusan')", 1200);
  $('#notif_surat_keputusan').load(site+'page/notifi/p_surat_keputusan');

  $(document).ajaxStart(function(){
      $("#pageload").show();
  });

  $(document).ajaxStop(function(){
    $("#pageload").hide();
    $(".price").on('keyup', function(){priceCurrency});priceCurrency();
    datepicker_format(); nav_url(); editor();

    $('#checkAll').click(function() {    
        $('input:checkbox').prop('checked', this.checked);    
    });
  });

  $('#myModal2').on('show', function() {
    $('#myModal').css('opacity', .5);
  });
  $('#myModal2').on('hidden', function() {
      $('#myModal').css('opacity', 1);
  });

  $('#checkAll').click(function() {    
      $('input:checkbox').prop('checked', this.checked);    
  });
});

var loading   = '<img src="'+site+'assets/loading.gif" style="margin-top:-3px;"> Please wait ..';

/*--------------MODAL--------------------------*/
function ajax_modals(title, dta, address, attrs) {
  if(attrs != undefined){$('#myModal').attr('style', attrs);} else{$('#myModal').removeAttr('style');}
  $('#myModalLabel').text(title);
  $('#modal-teks').html('');   
  $.ajax({
    type:'POST', data:dta, url:site+address, success:function(i){
      $('#modal-teks').html(i);
      $('#myModal').modal('show'); 
    }
  });
}
function ajax_modals2(title, dta, address, attrs) {
  if(attrs != undefined){$('#myModal2').attr('style', attrs);} else{$('#myModal2').removeAttr('style');}
  $('#myModalLabel2').text(title);
  $('#modal-teks2').html('');
  $.ajax({
    type:'POST', data:dta, url:site+address, success:function(i){
      $('#modal-teks2').html(i);
      $('#myModal2').modal('show');
    }
  });
}

/*--------------END MODAL--------------------------*/

/*--------------AJAX DATA--------------------------*/
function ajax_data(dta, address) {
  var result = "";
  $.ajax({
    type:'POST', data:dta, url:site+address, async:false, success:function(datas){
      result = datas;
    }
  });
  return result;
}

function val_data(url, bid, dta) {
  bid = bid == undefined ? 'primarycontent' : bid;
  var url = url.replace(site, '');

  $.ajax({
    type:'POST', data:dta, url:site+url, success:function(i) {
      $("#"+bid).val(i);
    }
  });
}

function eksekusi(url, bid, dta) {
  bid = bid == undefined ? 'primarycontent' : bid;
  var url = url.replace(site, '');

  $.ajax({
    type:'POST', data:dta, url:site+url, success:function(i) {
      $("#"+bid).html(i);
      
      if(window.location != site+url) {
        window.history.pushState({path: "+window.location+"}, "", site+url);
      }

    }
  });
}

function eksekusi_form(url, bid, dta) {
  bid = bid == undefined ? 'primarycontent' : bid;
  var url = url.replace(site, '');

  $.ajax({
    type:'POST', data:dta, url:site+url, success:function(i) {
      $("#"+bid).val(i);
      
      if(window.location != site+url) {
        window.history.pushState({path: "+window.location+"}, "", site+url);
      }

    }
  });
}

function aksi(url, bid, dta) {
  bid = bid == undefined ? 'primarycontent' : bid;

  $.ajax({
    type:'POST', data:dta, url:site+url, success:function(i) {
      $("#"+bid).html(i);
    }
  });
}

function tampil(url, tampil) {
  tampil = tampil == undefined ? 'primarycontent' : tampil;

  $("#"+tampil).load(site+url);

  if(window.location != site+url) {
    window.history.pushState({path: "+window.location+"}, "", site+url);
  }
}

function submit_form(formId, bidLoading, reload, mode, bid) {
  tinyMCE.triggerSave();
  if(formId != undefined) {
    var dta = $("#"+formId).serialize();
    var url_address = $("#"+formId).attr('action');
  } else {
    var dta = $('form').serialize();
    var url_address = $('form').attr('action');
  }

  bid = (bid == undefined) ? 'primarycontent' : bid;

  $('#'+bidLoading).html(loading);

  $.ajax({
    type:'POST',
    data:dta,
    url:url_address,
    success:function(i) {
      var pch = i.split("|||||");

      if(pch[0] == 'sukses') {

        if(reload != undefined)
        {
          $('#myModal').modal('hide');
          if(mode == 'eksekusi')
          {
            eksekusi(reload, bid);
          }
          else
          {
            $("#"+bid).load(site+reload);
          }
        }
        else
        {
          $('#'+bid).html(pch[1]);
          $('#myModal').modal('hide');
        }
        
      } else {

        alert(pch[0]);

      }

      $('#'+bidLoading).html('');
    }
  });
}

function submit_form_file(formId, bidLoading, reload) {
  tinyMCE.triggerSave();
  if(formId != undefined) {
    var url_address = $("#"+formId).attr('action');
  } else {
    var url_address = $('form').attr('action');
  }

  if(formId == 'formtambah_upload') {
    var fname = 'file_tambah';
  } else if(formId == 'formedit_upload') {
    var fname = 'file_edit';
  } else {
    var fname = 'file';
  }

  $.ajax({
    url: url_address,
    type: "POST",
    contentType: false,
    processData: false,
    data: function() {
      var data = new FormData();
      data.append("file", $("."+fname).get(0).files[0]);
      return data;
    }(),
    success: function(i){
      submit_form(formId, bidLoading, reload);
    }
  });
}

function submit_form_file2(formId, bidLoading, reload, div_id, mode, bid)
{
  tinyMCE.triggerSave();
    $('#' + bidLoading).html(loading);

    // Create the iframe...
    var iframe = document.createElement("iframe");
    iframe.setAttribute("id", "upload_iframe");
    iframe.setAttribute("name", "upload_iframe");
    iframe.setAttribute("width", "0");
    iframe.setAttribute("height", "0");
    iframe.setAttribute("border", "0");
    iframe.setAttribute("style", "width: 0px; height: 0px; border: none;display:none;");
 
    // Add to document...
    document.getElementById(formId).parentNode.appendChild(iframe);
    window.frames['upload_iframe'].name = "upload_iframe";
 
    iframeId = document.getElementById("upload_iframe");
 
    // Add event...
    var eventHandler = function () {
 
            if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
            else iframeId.removeEventListener("load", eventHandler, false);
 
            // Message from server...
            if (iframeId.contentDocument) {
                content = iframeId.contentDocument.body.innerHTML;
            } else if (iframeId.contentWindow) {
                content = iframeId.contentWindow.document.body.innerHTML;
            } else if (iframeId.document) {
                content = iframeId.document.body.innerHTML;
            }
 
            document.getElementById(div_id).innerHTML = content;
 
            // Del the iframe...
            setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);

            // penanganan
            var respon = $("#" + div_id).html();
            if(respon == 'sukses')
            {
              $('#myModal').modal('hide');
              if(mode == 'eksekusi')
              {
                eksekusi(reload, bid);
              }
              else if(mode == 'load')
              {
                location.href=site + reload;
              }
              else
              {
                if(bid != undefined) 
                {
                  $("#"+bid).load(site+reload);
                } 
                else 
                {
                  $("#primarycontent").load(site+reload);
                }
              }
            }
            else
            {
              alert(respon);
            }

            $('#'+bidLoading).html('');
        }
 
    if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
    if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);
 
    // Set properties of form with id = formId
    document.getElementById(formId).setAttribute("target", "upload_iframe");
    document.getElementById(formId).setAttribute("method", "post");
    document.getElementById(formId).setAttribute("enctype", "multipart/form-data");
    document.getElementById(formId).setAttribute("encoding", "multipart/form-data");
 
    // Submit form with id = formId
    document.getElementById(formId).submit();   
}

/*--------------END AJAX DATA--------------------------*/

/*--------------PLUGIN--------------------------*/
function priceCurrency() {
  $(".price").priceFormat({
    prefix: '',
    centsLimit: '0',
    thousandsSeparator: '.',
    allowNegative: true
  });
}

function datepicker_format() {
  $(".timepicker").timeEntry({
      show24Hours: true
    });
  
  $( ".datepicker" ).datepicker({
    changeMonth: true,
    changeYear: true,
    cache: false,
    dateFormat : "dd-mm-yy"
  });

  $( ".datepicker_from" ).datepicker({
    changeMonth: true,
    changeYear: true,
    cache: false,
    dateFormat : "dd-mm-yy",
    onClose: function( selectedDate ) {
      $( ".datepicker_to" ).datepicker( "option", "minDate", selectedDate );
    }
  });

  $( ".datepicker_to" ).datepicker({
    changeMonth: true,
    changeYear: true,
    cache: false,
    dateFormat : "dd-mm-yy",
    onClose: function( selectedDate ) {
      $( ".datepicker_from" ).datepicker( "option", "maxDate", selectedDate );
    }
  });  
}

function scroll_table() {
  $(function(){
    $('.table-scroll').tableScroll({
      flush: true,
      width: '100%',
      height: '235',
      containerClass: 'tablescroll'
    });
  });
}
/*--------------END PLUGIN--------------------------*/

function flashdata(nid)
{
  nid = nid == undefined ? 'msgshowhide' : nid;
  $("#" + nid).show().delay(2000).slideUp(500);
}

function nav_url() {
  var url     = ""+window.location+"";
  var url     = url.replace(""+site+"", "");
  var url     = url.split(undefined).join("");
  var link    = url.split("?")[0];
  var link    = link.split("/");

  var nlink1  = (link[0] == undefined) ? "" : link[0].split("_").join(" ");
  var nlink2  = (link[1] == undefined) ? "" : link[1].split("_").join(" ");
  var nlink3  = (link[2] == undefined) ? "" : link[2].split("_").join(" ");

  var link1   = '<button class="btn-link" style="font-weight:bold; text-transform:capitalize;" onClick=\'eksekusi("'+link[0]+'")\' title="'+nlink1+'">'+nlink1+'</button>';

  if(nlink2 == "")
    var link2   = "";
  else
    var link2   = '&raquo;<button class="btn-link" style="font-weight:bold; text-transform:capitalize;" onClick=\'eksekusi("'+link[0]+"/"+link[1]+'")\' title="'+nlink2+'">'+nlink2+'</button>';

  if(nlink3 == "") {

    var link3   = "";

  } else {

    var ptg = url.split(link[2]);

    if(ptg[1] == undefined)
      var link3   = '&raquo;<button class="btn-link" style="font-weight:bold; text-transform:capitalize;" onClick=\'eksekusi("'+link[0]+"/"+link[1]+"/"+link[2]+'")\' title="'+nlink3+'">'+nlink3+'</button>';
    else
      var link3   = '&raquo;<button class="btn-link" style="font-weight:bold; text-transform:capitalize;" onClick=\'eksekusi("'+link[0]+"/"+link[1]+"/"+link[2]+ptg[1]+'")\' title="'+nlink3+'">'+nlink3+'</button>';

  }

  var name_link   = link1+link2+link3;
  
  $("#hal_title").html(name_link);
} 

function popup_window(url, width, height) {
  var x = screen.width/2 - width/2;
  var y = screen.height/2 - height/2;
  window.open(url, '','height='+height+',width='+width+',left='+x+',top='+y);
}

function editor(val) {
  tinymce.init({
    selector: ".tinymce",
    skin : 'xenmce',
    resize : false,
    plugins: [
        "advlist autolink lists link charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime table contextmenu paste"
    ],
    toolbar: "styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"
  });
}