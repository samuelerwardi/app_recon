function slidePordukLaris()
{
  akhir = $('ul#produklaris li:last').hide().remove();
  $('ul#produklaris').prepend(akhir);
  $('ul#produklaris li:first').slideDown("slow");
}
setInterval(slidePordukLaris, 7000);

$(function() {
var ticker = $("#ticker");
ticker.children().filter("dt").each(function() {
  var dt = $(this),
    container = $("<div>");
  dt.next().appendTo(container);
  dt.prependTo(container);
  container.appendTo(ticker);
});
        
ticker.css("overflow", "hidden");
function animator(currentItem) {
  var distance = currentItem.height();
  duration = (distance + parseInt(currentItem.css("marginTop"))) / 0.025;
  currentItem.animate({ marginTop: -distance }, duration, "linear", function() {
  currentItem.appendTo(currentItem.parent()).css("marginTop", 0);
  animator(currentItem.parent().children(":first"));
  }); 
};
    
animator(ticker.children(":first"));
ticker.mouseenter(function() {
  ticker.children().stop();
});
    
ticker.mouseleave(function() {
  animator(ticker.children(":first"));
});
});
jQuery(document).ready(function($){
    $('#etalage').etalage({
        thumb_image_width: 300,
        thumb_image_height: 400,
        source_image_width: 900,
        source_image_height: 1200,
        show_hint: true,
        click_callback: function(image_anchor, instance_id){
            alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
        }
    });
});

$(window).load(function() {
      $("#flexiselDemo1").flexisel({
        visibleItems: 5,
        animationSpeed: 1000,
        autoPlay: true,
        autoPlaySpeed: 3000,        
        pauseOnHover: true,
        enableResponsiveBreakpoints: true,
          responsiveBreakpoints: { 
            portrait: { 
              changePoint:480,
              visibleItems: 1
            }, 
            landscape: { 
              changePoint:640,
              visibleItems: 2
            },
            tablet: { 
              changePoint:768,
              visibleItems: 3
            }
          }
        });
        
    });

// Scroll top
$(function() { $(window).scroll(function() {
    if($(this).scrollTop()>400) {
        $('#Back-to-top').fadeIn();
        }
    else { $('#Back-to-top').fadeOut();}
    });
    $('#Back-to-top').click(function() {
        $('body,html')
        .animate({scrollTop:0},300)
        .animate({scrollTop:40},200)
        .animate({scrollTop:0},130)
        .animate({scrollTop:15},100)
        .animate({scrollTop:0},70);
        });
});

// Scroll top end

  $(document).ready(function() {
  $('[data-toggle=offcanvas]').click(function() {
    $('.sidebar-offcanvas').toggleClass('active', 900);
  });
});

// sidemenu halaman customer
$(function(){

  $('#slide-submenu').on('click',function() {             
        $(this).closest('.list-group').fadeOut('slide',function(){
          $('.mini-submenu').fadeIn();  
        });
        
      });

  $('.mini-submenu').on('click',function(){   
        $(this).next('.list-group').toggle('slide');
        $('.mini-submenu').hide();
  })
});

$.getScript("http://www.urimalo.com/assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js", function(){

var startDate = new Date('01/01/2012');
var FromEndDate = new Date();
var ToEndDate = new Date();

ToEndDate.setDate(ToEndDate.getDate()+365);

$('.from_date').datepicker({
    
    weekStart: 1,
    startDate: '01/01/2012',
    endDate: FromEndDate, 
    autoclose: true
})
    .on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('.to_date').datepicker('setStartDate', startDate);
    }); 
$('.to_date')
    .datepicker({
        
        weekStart: 1,
        startDate: startDate,
        endDate: ToEndDate,
        autoclose: true
    })
    .on('changeDate', function(selected){
        FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $('.from_date').datepicker('setEndDate', FromEndDate);
    });

  
  
  
  
});

$(document).ready(function () {
      setTimeout(function () {
        $('.load-delay').each(function () {
            var imagex = $(this);
            var imgOriginal = imagex.data('original');
            $(imagex).attr('src', imgOriginal);
        });
    }, 900);
});