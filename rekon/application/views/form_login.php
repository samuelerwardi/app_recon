<?php $this->load->view('layout/pieces/header'); ?>

<body class="login-page">
    <div class="login-box">
      <div class="login-box-body">
        <div class="login-logo">
          <a href="<?php echo base_url(); ?>administrator"><img src="<?php echo base_url();?>assets/img/icon/icon-kc.png"><h3>Mitracomm Ekasarana</h3></a>
        </div><!-- /.login-logo -->
        <?php echo form_error('kode_captcha', '<div class="alert"><button type="button" class="close" data-dismiss="alert">&times;</button><span class="text-close data-dismiss="alert" aria-hidden="true">* ', '</span></div>'); ?>
        <?php if ($this->session->flashdata('pesan') != ''): ?>
              <p><div class="alert"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $this->session->flashdata('pesan'); ?></div></p>
            <?php endif ?>
        <br/>
        <!-- <p class="login-box-msg"><?php echo $title; ?></p> -->
        <form method="POST" id="login_form">
          <div class="form-group has-feedback">
            <input type="text" name="username" class="form-control-full" placeholder="Username"/>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="userpass" class="form-control-full" placeholder="Password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" name="kode_captcha" class="form-control-full" placeholder="Captcha" autocomplete="off"/><p></p>
            <span class="glyphicon glyphicon-link form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">    
              <p style="text-align:left"><?php echo $cap_img;?></p>                      
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" id="id" class="btn btn-primary btn-block btn-flat">LOGIN</button>
            </div><!-- /.col -->
          </div>
        </form>
        <hr/>
        <div class="text-center">
          <p class="muted credit" align='center'><font color='black'>Copyrights &copy; 2015 Aplikasi DB TOOLS</font><br/>
          <a href="#" style="text-decoration:none" title="Mitracomm Ekasarana &amp; "><font color='black'>Mitracomm Ekasarana</font></a></p> 
        </div>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
  </body>
</html>

<script type="text/javascript">
  $(document).ready(function(){
    $('#login_form').validate();
    $($document).on('click', '#btn-login', function(){
      var url = "<?php echo base_url();?>user/login";
      if($('#login_form').valid()){
        $.ajax({
          type : "POST",
          url  : url,
          data : $("#login_form").serialize(),
          success: function(data)
        });
      }
      return false;
    });
  });
</script>