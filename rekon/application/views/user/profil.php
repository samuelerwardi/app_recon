<?php echo form_open(current_url()); ?>

<?php echo validation_errors('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>', '</div>'); ?>

<table border=0 style="margin-top:10px;">
	<tr>
		<td width=110>Nama Lengkap</td>
		<td width=10>:</td>
		<td><?php echo form_input(array('class'=>'input-large', 'value'=>$dta->nama_user, 'name'=>'nama')); ?></td>
	</tr>
	<tr>
		<td>Username</td>
		<td>:</td>
		<td><?php echo form_input(array('class'=>'input-large', 'value'=>$dta->username, 'name'=>'username')); ?></td>
	</tr>
	<tr valign="bottom" height=40 align="right">
		<td colspan=3>
			<hr />
			<button class="btn btn-primary btn-small"><i class="icon-ok-sign icon-white"></i> Simpan</button>
			<input type="reset" value="Reset" class="btn btn-primary btn-small">
		</td>
	</tr>
</table>

<?php echo form_close(); ?>