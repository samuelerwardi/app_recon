<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="DB TOOL">
    <meta name="author" content="Mitracomm Ekasarana">
    <link href="<?php echo base_url(); ?>assets/img/icon/icon-kc.png" rel="shortcut icon" />
    <!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/mystyle.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/ui-themes/jquery.ui.base.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/ui-themes/jquery.ui.theme.css" rel="stylesheet">
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script language="javascript" src="<?php echo base_url()."assets/js/jquery.js";?>"></script>
    <script language="javascript" src="<?php echo base_url()."assets/js/ui/jquery.ui.core.js";?>"></script>
    <script language="javascript" src="<?php echo base_url()."assets/js/ui/jquery.ui.widget.js";?>"></script>
    <script language="javascript" src="<?php echo base_url()."assets/js/ui/jquery.plugin.js";?>"></script>
    <script language="javascript" src="<?php echo base_url()."assets/js/ui/jquery.timeentry.js";?>"></script>
    <script language="javascript" src="<?php echo base_url()."assets/js/jquery.price.js";?>"></script>
    <script language="javascript" src="<?php echo base_url()."assets/js/bootstrap-alert.js";?>"></script>
    <script language="javascript" src="<?php echo base_url()."assets/js/bootstrap-modal.js";?>"></script>
    <script language="javascript" src="<?php echo base_url()."assets/js/bootstrap-dropdown.js";?>"></script>
    <script language="javascript">var site='<?php echo base_url(); ?>'; var curr_site='<?php echo rtrim(current_url(), '/'); ?>';</script>
    <script language="javascript" src="<?php echo base_url()."assets/js/global.js";?>"></script>
    <script language="javascript" src="<?php echo base_url()."assets/js/ui/jquery.ui.datepicker.js";?>"></script>
    <script language="javascript" src="<?php echo base_url()."assets/js/jquery.tablescroll.js";?>"></script>
    <script type="text/javascript">  
    $(document).ready(function()
    {
      $('.ajax').click(function(e)
      {
        e.preventDefault();
        var url =  $(this).attr('href');
        eksekusi(url, 'primarycontent');
      });
    });
	</script>
  </head>

  <body>