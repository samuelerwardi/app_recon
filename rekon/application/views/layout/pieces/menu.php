      <aside class="main-sidebar">
        <section class="sidebar">
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url(); ?>assets/img/icon/avatar5.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?php echo $this->session->userdata('username'); ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
            <!-- search form -->
          <!-- /.search form -->
          <ul class="sidebar-menu">
            <li class="header">MENU NAVIGATION</li>
            <li>
           <!--  <li><a href="'.base_url().$row['url_module'].'" class="ajax" '.($row['url_module'] == NULL ? 'style="pointer-events:none"' : '').'><i class="'.$row['icon_module'].'" style="margin-top:-2px;"></i>&nbsp; '.$row['nama_module'].'</a>'.@$ci->template->dropdown_child($row).'</li> -->
              <a href="<?php echo base_url();?>home/">
                <i class="fa fa-home"></i> <span>Home</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Akses</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url();?>akses"><i class="fa fa-circle-o"></i> Data Akses</a></li>
                <li><a href="<?php echo base_url();?>akses/tambah"><i class="fa fa-circle-o"></i> Tambah Akses</a></li>
              </ul>
            </li>
            <li>
              <a href="<?php echo base_url();?>log_audit">
                <i class="fa fa-file-text-o"></i> <span>Log Audit Trail</span>
              </a>
            </li>
          </ul>
        </section>
      </aside>