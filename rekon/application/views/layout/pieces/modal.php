<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" id="closeModal" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="myModalLabel"></h3>
  </div>
  <div class="modal-body">
    <p id="error-message" style="color:#f00;font-size:13px;"></p>
    <p id="modal-teks"></p>
  </div>
</div>    
<div id="myModal2" class="modal hide submodal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" id="closeModal2" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="myModalLabel2"></h3>
  </div>
  <div class="modal-body">
    <p id="error-message2" style="color:#f00;font-size:13px;"></p>
    <p id="modal-teks2"></p>
  </div>
</div>