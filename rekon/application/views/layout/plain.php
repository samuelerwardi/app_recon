<?php $ci =& get_instance(); $ci->load->library('template'); ?>

<?php $this->load->view('layout/pieces/header') ?>
          
  <div class="back-bg">
    <div id="pageload" style="display:none;">
          <img src="<?php echo base_url(); ?>/assets/load-indicator.gif" style="margin-top:-3px;"> Loading, Please Wait ..
    </div> 
  </div> 
  
  <body class="hold-transition skin-yellow-light layout-top-nav layout-boxed">
    <div class="wrapper">    
      <!-- ini memanggil menu -->
      <?php $this->load->view('layout/pieces/header-top'); ?>
      <!-- end menu -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1><?php echo $title ; ?></h1>
          <!-- <ol class="breadcrumb">
            <a href="<?php echo base_url();?>home/"><i class="fa fa-home"></i></a>
              <li id="hal_title"></li>
          </ol> -->
          <div id="primarycontent">
            <?php echo $_content; ?> <!-- memanggil isi konten -->
          </div>
        </section>
        <!-- memanggil footer -->
        <?php $this->load->view('layout/pieces/footer'); ?>
        <!-- end footer -->
        <!-- memanggil modal -->
        <?php $this->load->view('layout/pieces/modal'); ?>
        <!-- end footer -->
    </div>
  </body>
</html>
<script language="javascript" src="<?php echo base_url()."assets/dist/js/app.min.js";?>"></script>