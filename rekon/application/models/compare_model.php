<?php
	class Compare_Model extends CI_Model{
		public function __construct(){
			parent::__construct();
			$this->config->load('compare_config');
			$this->local_config = $this->config->item('local_config');
			$this->mitra_config = $this->config->item('mitra_config');
		}
	
		public function execute($mitra, $additionalCondition = ""){
			$db_config = $this->mitra_config[$mitra]['db_config'];
			$query = $this->mitra_config[$mitra]['query'];

			if($additionalCondition != ""){
				$query .= $additionalCondition;
			}

			$this->load->database($db_config);
			$result = $this->db->query($query);
			return $result->result_array();
		}	

		public function check_from_config($mitra, $file_name, $check_from_history = false){
			$separator = $this->mitra_config[$mitra]['separator'];
			$compare_fields_txt = $this->mitra_config[$mitra]['compare_fields_txt'];
			$compare_fields_db = $this->mitra_config[$mitra]['compare_fields_db'];
			$fields_to_add_txt = $this->mitra_config[$mitra]['fields_to_add_txt'];
			$fields_to_add_db = $this->mitra_config[$mitra]['fields_to_add_db'];

			$this->check($mitra, $file_name, $separator, $compare_fields_txt, $compare_fields_db, $fields_to_add_txt, $fields_to_add_db, $check_from_history);
		}

		public function check($mitra, $file_name, $separator, $compare_fields_txt = array(), $compare_fields_db = array(), $fields_to_add_txt = array(), $fields_to_add_db = array(), $check_from_history = false){
	        $result = $this->execute($mitra);
	        $file_handle = fopen(base_url().'temp_upload/'.$file_name, "r");
	        while (!feof($file_handle) ) {//loop dri txt
	            $line_of_text = fgets($file_handle);
	            $parts = explode($separator, $line_of_text);
	            //buat nampung hasil compare
	            $row_result = array();

	            foreach ($result as $row) {//loop dari db
	            	$valids = array();
	                for($i = 0; $i < count($compare_fields_txt); $i++){
	                    if($row[$compare_fields_db[$i]] == $parts[$compare_fields_txt[$i]]){
	                    	$valids[] = true;
	                    }
	                }
	                if(count($valids) == count($compare_fields_txt)){
                	    $row_instance = array();
                    	foreach ($fields_to_add_txt as $field_before => $field_after) {
			                $row_instance[$field_after] = $parts[$field_before];
			            }
                        foreach ($fields_to_add_db as $field) {
                            $row_instance[$field] = $row[$field];
                        }
                        $row_result[] = $row_instance;
	                }
	            }
	            // ini terjadi jika txt tdak match dengan DB
	            if(!isset($row_result[0])){
	            	foreach ($fields_to_add_txt as $field_before => $field_after) {
		                $row_result[$field_after] = $parts[$field_before];
		            }
		        }
		        // switch database yang ke local
	            $this->load->database($this->local_config);
	            // Ini buat validasi Biller
	            if($check_from_history){
	            	if(!isset($row_result[0])){
			            $just_check = $this->db->query("SELECT 1 FROM history WHERE trxid = '" . $row_result['trxid'] . "'")->result_array();
			            if(count($just_check) > 0){
			            	$row_result['flag'] = 1; //	matching bank dan biller
			            	$row_result['tgl_proses']=date("Y-m-d");
			            	$this->db->update("history", $row_result, array("trxid" => $row_result["trxid"]));
			            }else{
			            	$row_result['flag'] = 2; //biller ada bank ga ada
			            	$row_result['tgl_proses']=date("Y-m-d");
			            	$this->db->insert("history", $row_result);
			            }
			        }else{
			        	// Jika multiline -> ketemu data ,tp result lbh dr 1 .ex : trxid nya lebih dari 1 di table .sedangkan di txt ,trxid tsb cuma 1
			        	foreach ($row_result as $row_instance) {
			        		$just_check = $this->db->query("SELECT 1 FROM history WHERE trxid = '" . $row_instance['trxid'] . "'")->result_array();
				            if(count($just_check) > 0){
				            	$row_instance['tgl_proses']=date("Y-m-d");
				            	$row_instance['flag'] = 1; //	matching bank dan biller
				            	$this->db->update("history", $row_instance, array("trxid" => $row_instance["trxid"]));
				            }else{
				            	$row_instance['tgl_proses']=date("Y-m-d");
				            	$row_instance['flag'] = 2; //biller ada bank ga ada
				            	$this->db->insert("history", $row_instance);
				            }
			        	}
			        }
		        }else{
		        	//jika data yang di compare 1
		        	if(!isset($row_result[0])){
			        	$row_result['flag'] = 3; //bank ada biller ga ada
			        	$row_result['tgl_proses']=date("Y-m-d");
		            	$this->db->insert("history", $row_result);
		            }
		            //Jika data yang di compare lebih dari 1
		            else{
		            	foreach ($row_result as $row) {
		            		$row['flag'] = 3;
		            		$row['tgl_proses']=date("Y-m-d");
		            		$this->db->insert("history", $row);
		            	}
		            }
	            }
	            // echo "<pre>";
	            // print_r($row_result);
	            // echo "</pre>";
	            // echo "<br/>";
	        }
	        // die;
	        fclose($file_handle);
	    }

		public function insert($table_name, $fields){			
			$this->load->database($this->local_config);
			$this->db->insert($table_name, $fields);
		}
		
	}
?>