<?php

// mengubah tgl format mySql menjadi dd-mm-yyyy
function dd_mm_yyyy($string)
{
	$pch 	= explode("-",$string);
	$format	= $pch[2]."-".$pch[1]."-".$pch[0];
	
	return $format;
}

function dd_mm_yyyy2($string)
{
	$pch 	= explode("-",$string);
	$format	= $pch[2]."/".$pch[1]."/".$pch[0];
	
	return $format;
}

function tgl_sql($string)
{
	$tgl = substr($string,0,2);
	$bln = substr($string,3,2);
	$thn = substr($string,6,4);
	
	return $thn."-".$bln."-".$tgl;
}

function tgl($string, $datetime = false, $timeonly = false)
{
	$tgl = substr($string,8,2);
	$bln = substr($string,5,2);
	$thn = substr($string,0,4);

	if($datetime == true) {
		$time = substr($string, 11);
	}

	if($datetime == true && $timeonly == true) {
		return $time;
	} else {
		return $tgl."-".$bln."-".$thn . ($datetime == true && $timeonly == false ? $time : '');	
	}

}

// mengubah tgl format mySql menjadi dd Januari yyyy
function tgl_indo($string)
{
	$tgl = substr($string,8,2);
	$bln = substr($string,5,2);
	$thn = substr($string,0,4);
	
	switch($bln)
	{
		case "01" : 
			$bulan = "Januari";
		break;
		
		case "02" : 
			$bulan = "Februari";
		break;
		
		case "03" : 
			$bulan = "Maret";
		break;
		
		case "04" : 
			$bulan = "April";
		break;
		case "05" : 
			$bulan = "Mei";
		break;
		
		case "06" : 
			$bulan = "Juni";
		break;
		
		case "07" : 
			$bulan = "Juli";
		break;
		
		case "08" : 
			$bulan = "Agustus";
		break;
		
		case "09" : 
			$bulan = "September";
		break;
		
		case "10" : 
			$bulan = "Oktober";
		break;
		
		case "11" : 
			$bulan = "November";
		break;
		
		case "12" : 
			$bulan = "Desember";
		break;
	}
	
	return $tgl." ".$bulan." ".$thn;
}

// mengubah tgl format mySql menjadi dd Januari yyyy
function sort_tgl_indo($string)
{
	$tgl = substr($string,8,2);
	$bln = substr($string,5,2);
	$thn = substr($string,0,4);
	
	switch($bln)
	{
		case "01" : 
			$bulan = "Jan";
		break;
		
		case "02" : 
			$bulan = "Feb";
		break;
		
		case "03" : 
			$bulan = "Mar";
		break;
		
		case "04" : 
			$bulan = "Apr";
		break;
		case "05" : 
			$bulan = "Mei";
		break;
		
		case "06" : 
			$bulan = "Jun";
		break;
		
		case "07" : 
			$bulan = "Jul";
		break;
		
		case "08" : 
			$bulan = "Agu";
		break;
		
		case "09" : 
			$bulan = "Sep";
		break;
		
		case "10" : 
			$bulan = "Okt";
		break;
		
		case "11" : 
			$bulan = "Nov";
		break;
		
		case "12" : 
			$bulan = "Des";
		break;
	}
	
	return $tgl." ".$bulan." ".$thn;
}

function bulan_indo($string)
{
	switch($string)
	{
		case "01" : 
			return "Januari";
		break;
		
		case "02" : 
			return "Februari";
		break;
		
		case "03" : 
			return "Maret";
		break;
		
		case "04" : 
			return "April";
		break;
		case "05" : 
			return "Mei";
		break;
		
		case "06" : 
			return "Juni";
		break;
		
		case "07" : 
			return "Juli";
		break;
		
		case "08" : 
			return "Agustus";
		break;
		
		case "09" : 
			return "September";
		break;
		
		case "10" : 
			return "Oktober";
		break;
		
		case "11" : 
			return "November";
		break;
		
		case "12" : 
			return "Desember";
		break;
	}
	
	return $bulan;
}

?>
