<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');  
/* 
| ------------------------------------------------------------------- 
| EMAIL CONFIG 
| ------------------------------------------------------------------- 
| Konfigurasi email keluar melalui mail server
| */  

$config['protocol']		='smtp';  
$config['smtp_host']	='ssl://smtp.mitracomm.com';  
$config['smtp_port']	='25';  
$config['smtp_timeout']	='30';  
$config['smtp_user']	='no_reply@dbtool.com';
$config['smtp_pass']	='';  
$config['charset']		='utf-8';  
$config['newline']		="\r\n";  

/* End of file email.php */ 
/* Location: ./system/application/config/email.php */