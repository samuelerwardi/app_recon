<?php 
	$config['local_config'] = array(
		'hostname' => "localhost",
		'username' => "root",
		'password' => "",
		'database' => "db_rekon",
		'dbdriver' => 'mysql',
		'dbprefix' => '',
		'pconnect' => TRUE,
		'db_debug' => TRUE,
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8',
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'autoinit' => TRUE,
		'stricton' => FALSE
	);

	$config['mitra_config'] = array(
		"PBB" => array(
			"db_config" => array(
				'hostname' => "localhost",
				'username' => "root",
				'password' => "",
				'database' => "db_rekon",
				'dbdriver' => 'mysql',
				'dbprefix' => '',
				'pconnect' => TRUE,
				'db_debug' => TRUE,
				'cache_on' => FALSE,
				'cachedir' => '',
				'char_set' => 'utf8',
				'dbcollat' => 'utf8_general_ci',
				'swap_pre' => '',
				'autoinit' => TRUE,
				'stricton' => FALSE
			),
			"query" => "SELECT * FROM trx_pbb",
			"separator" => ";",
			"compare_fields_txt" => array(0),
			"compare_fields_db" => array("trxid"),
			"fields_to_add_txt" => array(9 => "response",0=>"trxid"),
			"fields_to_add_db" => array("trxdt","trxtm","trxlocdate","trxtrcidfi","trxsvcid","trxprcd","trxmctp")
		),
		"BNI" => array(
			"db_config" => array(
				'hostname' => "localhost",
				'username' => "root",
				'password' => "",
				'database' => "db_rekon",
				'dbdriver' => 'mysql',
				'dbprefix' => '',
				'pconnect' => TRUE,
				'db_debug' => TRUE,
				'cache_on' => FALSE,
				'cachedir' => '',
				'char_set' => 'utf8',
				'dbcollat' => 'utf8_general_ci',
				'swap_pre' => '',
				'autoinit' => TRUE,
				'stricton' => FALSE
			),
			"query" => "SELECT * FROM trx",
			"separator" => ";",
			"compare_fields_txt" => array(2, 3),
			"compare_fields_db" => array("trxcustid", "trxamount"),
			"fields_to_add_txt" => array(2=>"trxcustno"),
			"fields_to_add_db" => array("trxid","trxdt","trxtm","trxlocdate","trxtrcidfi","trxsvcid","trxprcd","trxmctp")
		),
		"BCA" => array(
			"db_config" => array(
				'hostname' => "localhost",
				'username' => "root",
				'password' => "",
				'database' => "db_rekon",
				'dbdriver' => 'mysql',
				'dbprefix' => '',
				'pconnect' => TRUE,
				'db_debug' => TRUE,
				'cache_on' => FALSE,
				'cachedir' => '',
				'char_set' => 'utf8',
				'dbcollat' => 'utf8_general_ci',
				'swap_pre' => '',
				'autoinit' => TRUE,
				'stricton' => FALSE
			),
			"query" => "SELECT * FROM trx",
			"separator" => ";",
			"compare_fields_txt" => array(2, 3),
			"compare_fields_db" => array("trxcustid", "trxamount"),
			"fields_to_add_txt" => array(2=>"trxcustno"),
			"fields_to_add_db" => array("trxid","trxdt","trxtm","trxlocdate","trxtrcidfi","trxsvcid","trxprcd","trxmctp")
		)
	);
?>