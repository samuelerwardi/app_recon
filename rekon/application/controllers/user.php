<?php if(!defined('BASEPATH')) exit("No direct access script allowed");

class User extends CI_Controller
{
	
	function __construct() 
	{
		parent::__construct();
		$this->load->library('template');
		$this->load->model('dbm');
		$this->load->helper('captcha');

		$this->id = $this->session->userdata('id');
	}
	
	function index() 
	{ 
		redirect('login'); 
	}

	function login()
	{
		if($this->auth->is_logged_in() === TRUE )
		{
			redirect(base_url()."home");
		}
		
		$username = $this->input->post('username');
		$userpass = $this->input->post('userpass');

		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('userpass','Password','required');
		$this->form_validation->set_rules('kode_captcha', 'Kode Captcha', 'required|callback_cek_captcha');
		
		if($this->form_validation->run() == FALSE)
		{
			$cap 			 = $this->buat_captcha();
			$data['title']	 = 'Form Login Administrrator';
			$data['cap_img'] = $cap['image'];
			$this->session->set_userdata('kode_captcha',$cap['word']);

			$this->load->view('form_login',$data);
		}
		else
		{
			$cek_login = $this->auth->do_login($username,$userpass);
			if($cek_login == FALSE)
			{
				$this->session->set_flashdata('pesan', 'Username atau Password Anda salah, Silahkan ulangi lagi !!!');
				redirect('login');
			}
			else
			{
				$this->session->unset_userdata('kode_captcha');
				$ipadd	= $_SERVER['REMOTE_ADDR'];
				$info 	= "login with IP address: $ipadd";
				$id 	= $this->dbm->highest_id_plus_one('logid', 'log_audit');

				$data = array(
					// 'logid'			=> $id,
					'user'			=> $this->session->userdata('username'),
					'time' 			=> date('Y-m-d H:i:s'),
					'action'		=> "login",
					'information'	=> $info,
					);
				$insert = $this->dbm->insert('log_audit', $data);
				redirect(base_url().'home');
			}
		}
	}

	function buat_captcha()
	{
		$vals = array(
					  'img_path'  =>'./captcha/',
					  'img_url'   =>base_url().'captcha/',
					  'font_path' =>'./font/timesbd.ttf',
					  'img_width' =>'150',
					  'img_height'=>'30',
					  'expiration'=>'60' 
					  );
			  $cap = create_captcha($vals);

		return $cap;
	}

	function cek_captcha($input)
	{
		if($input === $this->session->userdata('kode_captcha'))
			{
				return TRUE;
			}
		else 
			{
   				$this->form_validation->set_message('cek_captcha', '%s yang anda input salah!');
   				return FALSE;
  			}
	}
	
	function logout()
	{
		$ipadd = $_SERVER['REMOTE_ADDR'];
		$info  = "logout with IP address: $ipadd";

		$id = $this->dbm->highest_id_plus_one('logid', 'log_audit');

		$data = array(
			// 'logid'			=> $id,
			'user'			=> $this->session->userdata('username'),
			'time' 			=> date('Y-m-d H:i:s'),
			'action'		=> "logout",
			'information'	=> $info,
			);

			$insert = $this->dbm->insert('log_audit', $data);
			if($insert == TRUE)
			{
				$unset = $this->session->unset_userdata(array(
				"id",
				"username",
				"userpass"
				));
		
		$this->session->sess_destroy();
		redirect('login');				
		}
	}
}
