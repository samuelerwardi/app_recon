<?php if (!defined('BASEPATH')) exit('No Direct script access allowed');

class Entri_data extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper('file');
        $this->load->model("compare_model");
        define('view', 'entri_data/');
    }

    function index()
    {
        $data['title'] = 'ENTRI DATA';
        $data['error'] = '';

        $this->template->display(view . 'form', $data);
    }

    function show_error(){
        $error = array('error' => $this->upload->display_errors());
        $error['title'] = 'ENTRI DATA';
        $this->template->display(view . 'form', $error);
    }


    function do_upload()
    {
        $config['upload_path'] = './temp_upload/';
        $config['allowed_types'] = 'xlsx|xls|txt';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
               $this->show_error(); 
        }else{
            $upload_data = $this->upload->data();
            $this->compare_model->check_from_config("BNI", $upload_data['file_name']);
        }

        if (!$this->upload->do_upload("userfile2")) {
               $this->show_error(); 
        }else{
            $upload_data = $this->upload->data();
            $this->compare_model->check_from_config("PBB", $upload_data['file_name'], true);
        }

        redirect("/entri_data");


        // if (!$this->upload->do_upload()) {
        //     $error = array('error' => $this->upload->display_errors());
        //     $error['title'] = 'ENTRI DATA';
        //     $this->template->display(view . 'form', $error);
        // } else {
        //     $data = array('upload_data' => $this->upload->data());
        //     $data['title'] = 'ENTRI DATA';
        //     $upload_data = $this->upload->data();
        //     $file_name = $upload_data['file_name'];
        //     $this->cek($file_name);
        // }

        // if (!$this->upload->do_upload("userfile2")) {
        //     $error = array('error' => $this->upload->display_errors());
        //     $error['title'] = 'ENTRI DATA';
        //     $this->template->display(view . 'form', $error);
        // } else {
        //     $data = array('upload_data' => $this->upload->data());
        //     $data['title'] = 'ENTRI DATA';
        //     $upload_data = $this->upload->data();
        //     $file_name = $upload_data['file_name'];
        //     $this->cek_biller($file_name);
        // }
    }


//     function cek($file_name)
//     {

//         $file_handle = fopen(base_url().'temp_upload/'.$file_name, "r");
//         while (!feof($file_handle) ) {

//             $line_of_text = fgets($file_handle);
//             $parts = explode(';', $line_of_text);
//             $partcustid = $parts[2];
//             $partamount = $parts[3];
//             $partreffid = $parts[4];
//             $partperiode = $parts[5];
//             $partcardno = $parts[6];
//             $parttermid = $parts[7];


//             $query = $this->dbm->insert('bank_tbl', array(
//                 'bank_trxid' => "",
//                 'bank_trxdate' =>  $partperiode,
//                 'bank_trxtime' => ""
//             ));

//             if($query == TRUE){
//                 $q_general = $this->dbm->insert('tbl_general', array(
//                     'custid'    => $partcustid,
//                     'custno'    => "",
//                     'custname'  => "",
//                     'periode'   => ""
//                     ));
//             }

// /*            $datax = $this->dbm->get_data_where('trx', array('trxcustid'=>$partcustid,'trxlocdate'=>$partperiode))->result();*/

//            /*echo $this->db->last_query();
//           die();*/
//   /*          if(empty($datax)){
//                 echo "data tidak cocok";
//             }else{

//             }*/
//         }
//         fclose($file_handle);
//     }


//     function cek_biller($file_name)
//     {

//         $file_handle = fopen(base_url().'temp_upload/'.$file_name, "r");
//         while (!feof($file_handle) ) {

//             $line_of_text = fgets($file_handle);
//             $parts = explode(';', $line_of_text);
//             $bill_trxid     = $parts[0];
//             $bill_datetime = $parts[1];
//             $bill_trxcusid = $parts[2];
//             $bill_trxperiod = $parts[3];
//             // $partcardno = $parts[];
//             $parttermid = $parts[10];
//             // print_r($parts[0]);

//             $q = $this->dbm->insert('tbl_biller', array(
//                 'biller_trxid'  => $bill_trxid,
//             ));

//             // if($query == TRUE){
//             //     $q_general = $this->dbm->insert('tbl_general', array(
//             //         'custid'    => $partcustid,
//             //         'custno'    => "",
//             //         'custname'  => "",
//             //         'periode'   => ""
//             //         ));
//             // }

// /*            $datax = $this->dbm->get_data_where('trx', array('trxcustid'=>$partcustid,'trxlocdate'=>$partperiode))->result();*/

//            /*echo $this->db->last_query();
//           die();*/
//   /*          if(empty($datax)){
//                 echo "data tidak cocok";
//             }else{

//             }*/
//         }
//         fclose($file_handle);
//     }

}

?>