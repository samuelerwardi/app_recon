<?php if(!defined('BASEPATH')) exit('no direct access script allowed');

class Page extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
  }

  function access_forbidden()
  {
  	$this->template->display('access_forbidden', array('title'=>'AKSES DITOLAK'));
  }
  
  function error()
  {
    $data['title'] = 'Halaman Tidak ditemukan';

    $this->template->display('404', $data);
  }
}