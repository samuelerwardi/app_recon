<?php if(!defined('BASEPATH')) exit('no direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->helper('other');
	}

	function index()
	{
		$data['title']		= 'Home';

		$this->template->display('home',$data);
	}	
}