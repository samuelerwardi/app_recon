<?php if(!defined('BASEPATH')) exit('No direct access script allowed');

class Auth
{
	var $CI = NULL;
	function __construct()
	{
		$this->CI = &get_instance();
	}
	
	// untuk validasi login
	function do_login($username,$userpass)
	{
		$this->CI->db->where('username',$username);
		$this->CI->db->where('userpass',md5($userpass));
		
		$result = $this->CI->db->get('admin');
		
		if($result->num_rows() == 0)
		{
			return FALSE;
		}
		else
		{
			$userdata = $result->row();
			$session_data = array(

				"id"		=> $userdata->id,
				"username"	=> $userdata->username,
				"userpass"	=> $userdata->userpass,
			);
			// buat session
			$this->CI->session->set_userdata($session_data);
			return TRUE;
		}
	}
	
	// cek apakah user login apa belum
	function is_logged_in()
	{
		if($this->CI->session->userdata('id') == '')
		{
			return FALSE;
		}
		return TRUE;
	}

}
