<?php
class Template {
	protected $_ci;
	function __construct()
	{
		$this->_ci=&get_instance();
	}
	
	function display($template,$data=null)
	{
		if(!$this->is_ajax())
		{
			$data['_content']	= $this->_ci->load->view($template,$data, true);
			$data['username'] 	= $this->_ci->session->userdata('username');
			$this->_ci->load->view('/layout/plain.php', $data);
		}
		else
		{
			$this->_ci->load->view($template,$data);
		}
	}

	function cetak($template, $data=null)	
	{
		$data['_isi']=$this->_ci->load->view($template,$data, true);
		$this->_ci->load->view('/template_print.php', $data);
	}

	function is_ajax()
	{
		return
		($this->_ci->input->server('HTTP_X_REQUESTED_WITH')&&($this->_ci->input->server('HTTP_X_REQUESTED_WITH')=='XMLHttpRequest'));
	}
}



