<?php if(!defined('BASEPATH')) exit('No direct access script allowed');

class Mypaging
{
    var $CI = NULL;
    public $set;
    public $select;
    public $config;

    function __construct()
    {
        $this->CI =& get_instance();
    }
    // Select query pagination
    function select($select = "*")
    {
        $this->select   = $select;
    }

    function configs($data=NULL, $jml=NULL, $dari=NULL, $select='')
    {   
        $this->CI->db->limit($jml, $dari);  

        $get    = $this->CI->db->select('SQL_CALC_FOUND_ROWS * ' . $select, FALSE)->get($data)->result();

        $count  = $this->CI->db->query("SELECT FOUND_ROWS() AS `count`")->row()->count;

        $result = array($get, $count);

        return $result;
    }
    
    // Config pagination
    function config($table = NULL, $page = NULL, $jml = '20')
    {   
        // Get halaman dan jml halaman
        $page   = $page-1;
        $dari   = $page*$jml;

        $select = ($this->select == null) ? ' *' : ' ' . $this->select;
        // Query data dengan limit
                $this->CI->db->limit($jml, $dari);  
        $get    = $this->CI->db->select('SQL_CALC_FOUND_ROWS' . $select, FALSE)->get($table)->result();

        // Menghitung jumlah data tanpa limit
        $count  = $this->CI->db->query("SELECT FOUND_ROWS() AS `count`")->row()->count;

        // Set setting pagination
        $this->set = $count .'|'. $jml .'|'. $page;

        return $get;
    }

    function created()
    {
        $set    = $this->set;
        $data   = explode('|', $set);

        $page   = $data[2];
        $pg     = $data[2]+1;

        $hal    = ceil($data[0]/$data[1]);
        $link   = str_replace(base_url(), '', current_url());
        $batas  = explode('/index/', $link);
        $link   = $batas[0];

        $cek    = preg_match('#\&#', $page[0]);

        $ask        = explode('?', $_SERVER['REQUEST_URI']);
        $link_ask   = (isset($ask[1])) ? '?'.$ask[1] : '';
        
        if($hal < 7)
            {
            $min = '1';
            $max = $hal;
            }
            else{
                $min = ($page > 3) ? $page-3 : '1';
                $max = ($page >= $hal-3) ? $hal : $page+3;
                }

        $start  = '<ul class="pagination"><li><a href="javascript:void(0);" onClick=\'aksi("'.$link.'/index/1'.$link_ask.'", "primarycontent")\' class="btn btn-default btn-sm'.($pg=='1' ? ' disabled' : '').'">&raquo;</a></li></ul>';
        $prev   = '<ul class="pagination"><li><a href="javascript:void(0);" onClick=\'aksi("'.$link.'/index/'.($page).''.$link_ask.'", "primarycontent")\' class="btn btn-default btn-sm'.($pg=='1' ? ' disabled' : '').'">&raquo;</a></li></ul> ';
        $last   = '<ul class="pagination"><li><a href="javascript:void(0);" onClick=\'aksi("'.$link.'/index/'.$hal.''.$link_ask.'", "primarycontent")\' class="btn btn-default btn-sm'.($pg==$hal ? ' disabled' : '').'">&raquo;</a></li></ul>';
        $next   = '<ul class="pagination"><li><a href="javascript:void(0);" onClick=\'aksi("'.$link.'/index/'.($pg+1).''.$link_ask.'", "primarycontent")\' class="btn btn-default btn-sm'.($pg==$hal ? ' disabled' : '').'">&raquo;</a></li></ul>';

        $paging = '';
        for($a=$min;$a<=$max;$a++)
            {
            $paging .= '<ul class="pagination"><li><a href="javascript:void(0);" onClick=\'aksi("'.$link.'/index/'.$a.''.$link_ask.'", "primarycontent")\' class="btn btn-default btn-sm'.($pg==$a ? ' disabled' : '').'">'.$a.'</a></li></ul>';
            }

        $p_hal = ($data[0]<$data[1]) ? '' : $start.$prev.' '.$paging.' '.$next.$last;

        return $p_hal;
    }

    function number()
    {
        $set    = $this->set;
        $data   = explode('|', $set);

        $page   = $data[2];

        $no     = ($page*$data[1])+1;

        return $no;
    }

    // Create pagination
    function create($ajax = FALSE, $index = 'index', $bid = 'primarycontent')
    {
        // Memecah data config
        $set    = $this->set;
        $data   = explode('|', $set);

        // Set halaman
        $page   = $data[2];
        $pg     = $data[2]+1;

        // Menghitung berapa pagiantionya
        $hal    = ceil($data[0]/$data[1]);

        // Menselect data yang dikirim lewat url
        $link   = str_replace(base_url(), '', current_url());
        $batas  = explode('/'.$index, $link);
        $link   = $batas[0];

        // Menselect get data
        $ask        = explode('?', $_SERVER['REQUEST_URI']);
        $link_ask   = (isset($ask[1])) ? '?'.$ask[1] : '';
        
        // Membatasi jumlah pagination
        if($hal < 7)
        {
            $min = '1';
            $max = $hal;
        }
        else
        {
            $min = ($page > 3) ? $page-3 : '1';
            $max = ($page >= $hal-3) ? $hal : $page+3;
        }

        // Jika menggunakan ajax
        if($ajax == TRUE)
        {
            $start  = '<ul class="pagination">
                            <li><a href="javascript:void(0);" onClick=\'aksi("'. $link .'/'. $index .'/1'. $link_ask .'", "' . $bid . '")\' class="btn btn-default btn-sm'. ($pg == '1' ? ' disabled' : '') .'">&raquo;</a></li>
                        </ul>';
            $prev   = '<ul class="pagination">
                            <li><a href="javascript:void(0);" onClick=\'aksi("'. $link .'/'. $index .'/'. ($page) . $link_ask.'", "' . $bid . '")\' class="btn btn-default btn-sm'. ($pg == '1' ? ' disabled' : '') .'">&raquo;</a></li>
                        </ul>';
            $last   = '<ul class="pagination">
                            <li><a href="javascript:void(0);" onClick=\'aksi("'. $link .'/'. $index .'/'. $hal . $link_ask.'", "' . $bid . '")\' class="btn btn-default btn-sm'. ($pg == $hal ? ' disabled' : '') .'">&raquo;</i></a></li>
                        </ul>';
            $next   = '<ul class="pagination">
                            <li><a href="javascript:void(0);" onClick=\'aksi("'. $link .'/'. $index .'/'. ($pg+1) . $link_ask.'", "' . $bid . '")\' class="btn btn-default btn-sm'. ($pg == $hal ? ' disabled' : '') .'">&raquo;</i></a></li>
                        </ul>';

            $paging = '';

            for($a=$min; $a<=$max;$a++)
            {
                $paging .= '<a href="javascript:void(0);" onClick=\'aksi("'. $link .'/'. $index .'/'. $a . $link_ask .'", "' . $bid . '")\' class="btn btn-default btn-sm'. ($pg == $a ? ' disabled' : '') .'">'. $a .'</a>';
            }
        }
        // Jika tidak menggunakan ajax
        elseif($ajax == FALSE)
        {
            $start  = '<ul class="pagination">
                            <li><a href="javascript:void(0);" onClick=\'location.href="'. base_url().$link .'/'. $index .'/1'. $link_ask .'"\' class="btn btn-default btn-sm'. ($pg == '1' ? ' disabled' : '') .'">&raquo;</a></li>
                        </ul>';
            $prev   = '<ul class="pagination">
                            <li><a href="javascript:void(0);" onClick=\'location.href="'. base_url().$link .'/'. $index .'/'. ($page) . $link_ask.'"\' class="btn btn-default btn-sm'. ($pg == '1' ? ' disabled' : '') .'">&raquo;</a></li>
                        </ul> ';
            $last   = '<ul class="pagination">
                            <li><a href="javascript:void(0);" onClick=\'location.href="'. base_url().$link .'/'. $index .'/'. $hal . $link_ask.'"\' class="btn btn-default btn-sm'. ($pg == $hal ? ' disabled' : '') .'">&raquo;</a></li>
                        </ul>';
            $next   = '<ul class="pagination">
                            <li><a href="javascript:void(0);" onClick=\'location.href="'. base_url().$link .'/'. $index .'/'. ($pg+1) . $link_ask.'"\' class="btn btn-default btn-sm'. ($pg == $hal ? ' disabled' : '') .'">&raquo;</a></li>
                        </ul>';

            $paging = '';

            for($a=$min; $a<=$max; $a++)
            {
                $paging .= '<ul class="pagination">
                            <li><a href="javascript:void(0);" onClick=\'location.href="'. base_url() . $link .'/'. $index .'/'. $a . $link_ask .'"\' class="btn btn-default btn-sm'. ($pg == $a ? ' disabled' : '') .'">'. $a .'</a></li>
                            </ul>';
            }
        }

        return ($data[0] <= $data[1]) ? '' : $start.$prev .' '. $paging .' '. $next.$last;
    }

    function set($data=NULL, $jml=NULL, $page=NULL, $count_data=NULL)
    {
        $this->set = $count_data.'|'.$jml.'|'.$page;
    }

    // Create nomor untuk list pagination
    function nomor()
    {
        $set    = $this->set;
        $data   = explode('|', $set);

        $page   = $data[2];

        $no     = ($page*$data[1])+1;

        return $no;
    }

}
